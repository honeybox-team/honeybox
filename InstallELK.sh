#!/bin/bash

# Colors ##########################################################################
CSI="\\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"
###################################################################################

# General functions ###############################################################
function basicSetup {
  # Install required apt & pip packages
  apt-get install nano python3 python3-pip apt-transport-https tzdata unzip sshpass -y
}

function ufwSetup {
	# Create inbound traffic rules and enable UFW Firewall
  ufw allow 80/tcp comment "HTTP"
  ufw allow 443/tcp comment "HTTPS"
  ufw allow 22/tcp comment "SSH"
  ufw allow 5601/tcp comment "Kibana"
  ufw allow 9200/tcp comment "ElasticSearch"
  ufw allow 5044/tcp comment "Logstash"
  ufw allow 1234/tcp comment "Webinterface"
  ufw enable
}

function nginxSetup {
	# Add official nginx package repository and install nginx
  add-apt-repository ppa:nginx/mainline -y
  apt-get update -y
  apt-get install nginx -y
  systemctl stop nginx
  cp /home/ubuntu/honeybox/conf/default-http /etc/nginx/sites-enabled/default
  sed -i "s/ssl_protocols TLSv1/#ssl_protocols TLSv1/g" /etc/nginx/nginx.conf
  sed -i "s/ssl_prefer_server_ciphers on;/#ssl_prefer_server_ciphers on;/g" /etc/nginx/nginx.conf
  systemctl start nginx
}

function acmeSetup {
    # Setup the acme.sh client for requesting TLS certs from LetsEncrypt
    adduser acme # Choose random PW for user
    sudo usermod -a -G www-data acme
    echo "acme ALL=NOPASSWD: /bin/systemctl reload nginx.service" | (EDITOR="tee -a" visudo)
    su -c 'cd /home/acme && git clone https://github.com/Neilpang/acme.sh.git && cd acme.sh && ./acme.sh --install --home /home/acme/.acme.sh' acme

    mkdir -p /var/www/acme/.well-known/acme-challenge
    chown -R www-data:www-data /var/www/acme
    chmod -R 775 /var/www/acme
    mkdir -p /etc/acme/
    chown -R www-data:www-data /etc/acme/
    chmod -R 775 /etc/acme/
}

function elasticSetup {
    # Downloa and Install official ElasticSearch binaries via apt
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
    echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
    apt-get update && apt-get install elasticsearch=7.5.2 -y
    /bin/systemctl daemon-reload
    /bin/systemctl enable elasticsearch.service
    sed -i "s/#network.host: 192.168.0.1/network.host: 127.0.0.1/g" /etc/elasticsearch/elasticsearch.yml
    sed -i "s/#http.port: 9200/http.port: 9201/g" /etc/elasticsearch/elasticsearch.yml
    sed -i "s/#node.name: node-1/node.master: true\nnode.data: true/g" /etc/elasticsearch/elasticsearch.yml
    systemctl start elasticsearch
}

function kibanaSetup {
    # Downloa and Install official Kibana binaries via apt
    apt-get install kibana=7.5.2 -y
    /bin/systemctl daemon-reload
    /bin/systemctl enable kibana.service
    sed -i "s/#server.port: 5601/server.port: 5602/g" /etc/kibana/kibana.yml
    sed -i 's/#server.host: "localhost"/server.host: "127.0.0.1"/g' /etc/kibana/kibana.yml
    sed -i 's/#server.name: "your-hostname"/server.name: "Honeybox ELK Stack"/g' /etc/kibana/kibana.yml
    sed -i "s/localhost:9200/127.0.0.1:9201/g" /etc/kibana/kibana.yml
    sed -i "s/#elasticsearch.hosts:/elasticsearch.hosts:/g" /etc/kibana/kibana.yml
    systemctl start kibana
}

function logstashSetup {
    # Download and Install official Logstash binaries via apt + Install Java 11
    apt-get install openjdk-11-jre -y
    apt-get install logstash -y
    /bin/systemctl daemon-reload
    /bin/systemctl enable logstash.service
    cp /home/ubuntu/honeybox/conf/logstash-input /etc/logstash/conf.d/00-inputs.conf
    cp /home/ubuntu/honeybox/conf/logstash-mqtt /etc/logstash/conf.d/11-filter_mqtt.conf
    cp /home/ubuntu/honeybox/conf/logstash-mariadb /etc/logstash/conf.d/12-filter_mariadb.conf
    cp /home/ubuntu/honeybox/conf/logstash-openssh /etc/logstash/conf.d/13-filter_openssh.conf
    cp /home/ubuntu/honeybox/conf/logstash-output /etc/logstash/conf.d/50-outputs.conf
    systemctl start logstash
}

function elastalertSetup {
  pip3 install PyYAML==5.1 elastalert
  git clone https://github.com/yelp/elastalert /home/ubuntu/elastalert-bin
  cd /home/ubuntu/elastalert-bin || exit
  cp /home/ubuntu/honeybox/conf/elastalert-config /home/ubuntu/elastalert-bin/config.yaml
  systemctl start elasticsearch
  elastalert-create-index
  cat <<EOM > /etc/systemd/system/elastalert.service
[Unit]
Description=elastalert
After=elasticsearch.service

[Service]
Type=simple
WorkingDirectory=/home/ubuntu/elastalert-bin
ExecStart=/usr/local/bin/elastalert --verbose --config /home/ubuntu/elastalert-bin/config.yaml

[Install]
WantedBy=multi-user.target
EOM
  systemctl daemon-reload
  systemctl enable elastalert
  systemctl stop elastalert
}

function webSetup {
  pip3 install -r /home/ubuntu/honeybox/webinterface/requirements.txt
  cp /home/ubuntu/honeybox/webinterface/sample.config.ini /home/ubuntu/honeybox/webinterface/config.ini
  chown ubuntu:ubuntu /home/ubuntu/honeybox/webinterface/config.ini
  KEY=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24)
  sed -i "s/secret_key = ReplaceHereForSecurity!!/secret_key = $KEY/g" /home/ubuntu/honeybox/webinterface/config.ini
  cat <<EOM > /etc/systemd/system/webinterface.service
[Unit]
Description=Webinterface
After=docker.service

[Service]
Type=simple
Environment=FLASK_APP=/home/ubuntu/honeybox/webinterface/app.py
WorkingDirectory=/home/ubuntu/honeybox/webinterface
ExecStart=/usr/bin/python3 -m flask run --host=0.0.0.0 --port=8000

[Install]
WantedBy=multi-user.target
EOM
  systemctl daemon-reload
  systemctl enable webinterface
  systemctl start webinterface
}

function setPorts {
  # Change the routable ports on nginx/Logstash for the available services
  # Elasticsearch no TLS
  sed -i -E "s/listen ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]);#elastic/listen $1;#elastic/g" /etc/nginx/sites-available/default
  sed -i -E "s/:([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]);#elastic/:$1;#elastic/g" /etc/nginx/sites-available/default
  # Elasticsearch with TLS
  sed -i -E "s/listen ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]) ssl http2;#elastic/listen $1 ssl http2;#elastic/g" /etc/nginx/sites-available/default
  sed -i -E "s/:([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]) ssl http2;#elastic/:$1 ssl http2;#elastic/g" /etc/nginx/sites-available/default
  # Kibana no TLS
  sed -i -E "s/listen ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]);#kibana/listen $2;#kibana/g" /etc/nginx/sites-available/default
  sed -i -E "s/:([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]);#kibana/:$2;#kibana/g" /etc/nginx/sites-available/default
  # Kibana with TLS
  sed -i -E "s/listen ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]) ssl http2;#kibana/listen $2 ssl http2;#kibana/g" /etc/nginx/sites-available/default
  sed -i -E "s/:([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]) ssl http2;#kibana/:$2 ssl http2;#kibana/g" /etc/nginx/sites-available/default
  # Logstash
  sed -i -E "s/port => ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])/port => $3/g" /etc/logstash/conf.d/00-inputs.conf
  # Delete old firewall rules
  #OLD_ELASTIC_PORT=$(grep -E -o "listen ([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]);#elastic" /etc/nginx/sites-available/default)
  # Add new firewall rules
  ufw allow "$1"/tcp comment "ElasticSearch"
  ufw allow "$2"/tcp comment "Kibana"
  ufw allow "$3"/tcp comment "Logstash"
  systemctl restart nginx logstash
  echo "Ports set!"
}

function createPasswords {
  # Create credentials with the automatic password utility from elastic
  echo "xpack.security.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
  systemctl restart elasticsearch
  yes | /usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto > /tmp/output
  elasticCreds=$(grep "elastic = [A-Za-z0-9]*" /tmp/output )
  elasticPW=${elasticCreds: -20}
  kibanaCreds=$(grep "kibana = [A-Za-z0-9]*" /tmp/output )
  kibanaPW=${kibanaCreds: -20}
  # Configure Kibana with elastic credentials
  sed -i 's/#elasticsearch.username: "kibana"/elasticsearch.username: "kibana"/g' /etc/kibana/kibana.yml
  sed -i 's/#elasticsearch.password: "pass"/elasticsearch.password: '"$kibanaPW"'/g' /etc/kibana/kibana.yml
  # Configure Logstash with elastic credentials
  sed -i 's/#user => "elastic"/user => "elastic"/g' /etc/logstash/conf.d/50-outputs.conf
  sed -i 's/#password => "elasticPWoutput"/password => '"$elasticPW"'/g' /etc/logstash/conf.d/50-outputs.conf
  # Write elastic password to webinterface config file
  sed -i 's/elasticpw = elasticDefaultPW/elasticpw = '"$elasticPW"'/g' /home/ubuntu/honeybox/webinterface/config.ini
  # Write elastic password to elastalert config
  sed -i "s/#es_password: elasticpassword/es_password: $elasticPW/g" /home/ubuntu/elastalert-bin/config.yaml
  sed -i "s/#es_username: elastic/es_username: elastic/g" /home/ubuntu/elastalert-bin/config.yaml
  systemctl restart elasticsearch kibana logstash
  rm /tmp/output
}

function createCACerts {
  # Create self-signed CA certs for the Logstash connection
  # mkdir /home/ubuntu/honeybox/ca
  /usr/share/elasticsearch/bin/elasticsearch-certutil ca --out /home/ubuntu/honeybox/ca.zip --pass honeybox --days 3650 --pem
  unzip -o -d /home/ubuntu/honeybox/ /home/ubuntu/honeybox/ca.zip
}

function createClientCerts {
  # Create self-signed server/client certs for the Logstash connection
  # 1=Domain
  /usr/share/elasticsearch/bin/elasticsearch-certutil cert --out /home/ubuntu/honeybox/ca/elk-logstash.zip --ca-cert /home/ubuntu/honeybox/ca/ca.crt --ca-key /home/ubuntu/honeybox/ca/ca.key --ca-pass honeybox --pem --dns "$1"
  /usr/share/elasticsearch/bin/elasticsearch-certutil cert --out /home/ubuntu/honeybox/ca/elk-filebeat.zip --ca-cert /home/ubuntu/honeybox/ca/ca.crt --ca-key /home/ubuntu/honeybox/ca/ca.key --ca-pass honeybox --pem --dns "$1"
  #cd /home/ubuntu/honeybox/ca || exit
  unzip -o -d /home/ubuntu/honeybox/ca/ /home/ubuntu/honeybox/ca/elk-logstash.zip
  mv /home/ubuntu/honeybox/ca/instance/instance.crt /home/ubuntu/honeybox/ca/logstash.crt
  mv /home/ubuntu/honeybox/ca/instance/instance.key /home/ubuntu/honeybox/ca/logstash.key
  openssl pkcs8 -in /home/ubuntu/honeybox/ca/logstash.key -topk8 -nocrypt -out /home/ubuntu/honeybox/ca/logstash.p8
  rm -r /home/ubuntu/honeybox/ca/instance
  unzip -o -d /home/ubuntu/honeybox/ca/ /home/ubuntu/honeybox/ca/elk-filebeat.zip
  mv /home/ubuntu/honeybox/ca/instance/instance.crt /home/ubuntu/honeybox/ca/filebeat.crt
  mv /home/ubuntu/honeybox/ca/instance/instance.key /home/ubuntu/honeybox/ca/filebeat.key
  openssl pkcs8 -in /home/ubuntu/honeybox/ca/filebeat.key -topk8 -nocrypt -out /home/ubuntu/honeybox/ca/filebeat.p8
  rm -r /home/ubuntu/honeybox/ca/instance
  chown ubuntu:ubuntu /home/ubuntu/honeybox/ca/*
  chmod 775 /home/ubuntu/honeybox/ca/*
}

function createTLSCerts {
  # Request Certs from letsencrypt via HTTP-01 challenge
  # 1=Domain
  systemctl start nginx
  su -c '/home/acme/acme.sh/acme.sh --issue -d '"$1"' --keylength ec-256 -w /var/www/acme --key-file /etc/acme/key.pem --ca-file /etc/acme/ca.pem --cert-file /etc/acme/cert.pem --fullchain-file /etc/acme/fullchain.pem --reloadcmd "sudo /bin/systemctl reload nginx.service" --force' acme
}

function logstashAuthConf {
  sed -i 's/#ssl => true/ssl => true/g' /etc/logstash/conf.d/00-inputs.conf
  sed -i 's/#ssl_certificate_authorities/ssl_certificate_authorities/g' /etc/logstash/conf.d/00-inputs.conf
  sed -i 's/#ssl_certificate /ssl_certificate /g' /etc/logstash/conf.d/00-inputs.conf
  sed -i 's/#ssl_key/ssl_key/g' /etc/logstash/conf.d/00-inputs.conf
  systemctl restart logstash
}

function nginxTLSConf {
  cp /home/ubuntu/honeybox/conf/default-https /etc/nginx/sites-available/default
  cp /home/ubuntu/honeybox/conf/default-tls /etc/nginx/conf.d/tls.conf
  sed -i "s/server_name elk.sdv.xyz/server_name $1/g" /etc/nginx/sites-available/default
  systemctl restart nginx
}

function telegramConf {
  # Define telegram bot token
  sed -i -E 's/[0-9]{9}:[a-zA-Z\-\_]{35}/'"$1"'/g' /home/ubuntu/honeybox/elastalert/rules/*
  # Define Telegram bot room
  sed -i -E 's/-[0-9]{9}/'"$2"'/g' /home/ubuntu/honeybox/elastalert/rules/*
  systemctl restart elastalert
}


###################################################################################

# Code when run headless ##########################################################
if [ "$1" == "headless" ]; then
    # Switch-case to the desired functionality
    case $2 in
        "config") # Configure stuff
            if [ "$3" == "auth" ]; then
              createPasswords
            fi
            if [ "$3" == "ports" ]; then
              setPorts "$4" "$5" "$6"
            fi
            if [ "$3" == "tls" ]; then
              createCACerts
              createClientCerts "$4"
              createTLSCerts "$4"
              logstashAuthConf
              nginxTLSConf "$4"
            fi
            if [ "$3" == "telegram" ]; then
              # 4 bot token, 5 room id
              telegramConf "$4" "$5"
            fi
        exit
        ;;
        
        "start") # Start services headlessly
            systemctl restart elasticsearch kibana logstash nginx
        exit
        ;;

        "stop") # Stop services headlessly
            systemctl stop elasticsearch kibana logstash nginx
        exit
        ;;
        esac
fi
###################################################################################

# Menu ############################################################################
clear
echo ""
echo -e "${CGREEN}Welcome to the ELK installation script!${CEND}"
echo ""
echo "What do you want to do?"
echo "   1) Install nginx, acme, Elasticsearch, Logstash, Kibana, elastalert"
echo "   2) Start nginx, Elasticsearch, Logstash, Kibana, elastalert"
echo "   3) Stop nginx, Elasticsearch, Logstash, Kibana, elastalert"
echo -e "${CRED}   9) Exit${CEND}"
echo ""

while [[ $OPTION !=  "1" && $OPTION !=  "2" && $OPTION !=  "3" && $OPTION !=  "9" ]]; 
do
	read -rp "Select an option [1-9]: " OPTION
done
###################################################################################

# Switch case #####################################################################
case $OPTION in
	1)  # Install everything
      basicSetup
      ufwSetup
      nginxSetup
      acmeSetup
      elasticSetup
      logstashSetup
      kibanaSetup
      elastalertSetup
      webSetup
	exit
    ;;

	2)  # Start everything
      systemctl start elasticsearch logstash kibana nginx
	exit
	;;

	3)  # Stop everything
      systemctl stop elasticsearch logstash kibana nginx
	exit
	;;

  4)  # Set service ports
      # Get Ports
      read -rp "Please set the PORT for your ElasticSearch instance: [9200] " ELASTIC_PORT
      ELASTIC_PORT=${ELASTIC_PORT:-9200}
      read -rp "Please set the PORT for your Kibana instance: [5601] " KIBANA_PORT
      KIBANA_PORT=${KIBANA_PORT:-5601}
      read -rp "Please set the PORT for your Logstash Beats Input: [5044] " LOGSTASH_PORT
      LOGSTASH_PORT=${LOGSTASH_PORT:-5044}
      setPorts "$ELASTIC_PORT" "$KIBANA_PORT" "$LOGSTASH_PORT"
	exit
	;;
	
	9)
		clear
    exit
	;;
esac
###################################################################################
