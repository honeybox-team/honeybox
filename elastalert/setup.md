# Install ElastAlert Kibana Plugin
Inside this guide we will show you how to install the ElastAlert Plugin.

## Install ElastAlert Server with Docker
```
git clone https://github.com/bitsensor/elastalert.git
```
```
cd elastalert
```

### Execute following command (we are using the beta verison in this case)
```
sudo docker run -d -p 3030:3030 \
    -v `pwd`/config/elastalert.yaml:/opt/elastalert/config.yaml \
    -v `pwd`/config/config.json:/opt/elastalert-server/config/config.json \
    -v `pwd`/rules:/opt/elastalert/rules \
    -v `pwd`/rule_templates:/opt/elastalert/rule_templates \
    --net="host" \
    --name elastalert bitsensor/elastalert:3.0.0-beta.0
```

You can check with `sudo docker ps -a` if the container is up and running.
And you can check the logs with `sudo docker logs CONTAINER-ID`


---

## Install the ElastAlert Plugin
Get the right version:
https://github.com/bitsensor/elastalert-kibana-plugin/releases
In this example we are using kibana 7.4.2

Run following command:
./bin/kibana-plugin install https://github.com/bitsensor/elastalert-kibana-plugin/releases/download/1.1.0/elastalert-kibana-plugin-1.1.0-7.4.2.zip

Restart Kibana:
`sudo systemctl restart kibana.service`

If you try to create a rule and receive a `internal 500 server error`message you can fix this by change the rights:
`sudo chmod -R o+rw rules` inside the location where you have started / build the docker image from above