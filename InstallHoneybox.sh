#!/bin/bash

# Variables #######################################################################
SCRIPT_PATH="$(pwd)"
HOST_IP="0.0.0.0"
PATH="/home/ubuntu/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
DOCKER_VERSION="19.03.6"
###################################################################################

# Colors ##########################################################################
CSI="\\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CYELLOW="${CSI}1;33m"
CGREEN="${CSI}1;32m"
###################################################################################

# Functions #######################################################################
function startService {
# Start the provided service stack
    cd "$SCRIPT_PATH"/services/"$1"/ || exit
    if ! (docker-compose up -d) 2> /dev/null; then
        return 1
    else
        return 0
    fi
}

function startAllServices {
# Start all of the available services
    startService mosquitto
    startService mariadb
    startService snmpd
    startService openssh
}

function stopService {
# Stop the provided service stack
    cd "$SCRIPT_PATH"/services/"$1"/ || exit
    if ! (docker-compose down) 2> /dev/null; then
        return 1
    else
        return 0
    fi
}

function stopAllServices {
# Stop all of the available services
    stopService mosquitto
    stopService mariadb
    stopService snmpd
    stopService openssh
}

function resetService {
# Reset the provided service stack
    if [ "$1" == "mosquitto" ]; then
        stopService mosquitto
        rm -rf "$SCRIPT_PATH"/services/mosquitto/log/*
        if ! startService mosquitto; then
            return 1
        else
            return 0
        fi
    fi
    if [ "$1" == "mariadb" ]; then
        stopService mariadb
        sudo rm -rf "$SCRIPT_PATH"/services/mariadb/data/*
        if ! startService mariadb; then
            return 1
        else
            return 0
        fi
    fi
    if [ "$1" == "snmpd" ]; then
        stopService snmpd
        rm -rf "$SCRIPT_PATH"/services/snmpd/log/*
        if ! startService snmpd; then
            return 1
        else
            return 0
        fi
    fi
    if [ "$1" == "openssh" ]; then
        stopService openssh
        rm -rf "$SCRIPT_PATH"/services/openssh/log/*
        if ! startService openssh; then
            return 1
        else
            return 0
        fi
    fi
}

function resetAllServices {
# Reset all of the available services
    resetService mosquitto
    resetService mariadb
    resetService snmpd
    resetService openssh
}

function buildImage {
# Build the corresponding docker image of the provided service
    cd "$SCRIPT_PATH"/services/"$1"/build || exit
    if ! (docker build -t honeybox/"$1" .) > /dev/null; then
        echo -e "[$1] Failed to build Docker image"
    fi
}

function updateService {
# Pull the latest service image from Docker Hub
    cd "$SCRIPT_PATH"/services/"$1"/ || exit
    if ! (docker-compose pull) > /dev/null; then
        return 1
    else
        startService "$1"
        return 0
    fi
}

function updateAllServices {
# Pull the latest service images
    updateService mosquitto
    updateService mariadb
    updateService snmpd
    updateService openssh
}

function healthcheck {
# Check if a specific service is actually running
    if [ "$(docker inspect -f "{{.State.Running}}" "$1" 2>/dev/null)" ]; then
        echo "running"
    else
        >&2 echo "stopped"
    fi
}

function installBeats {
# Installing beats
    "$SCRIPT_PATH"/beats/installBeats.sh "$1"
}

function configBeats {
# Configure ELK Host, Port and Credentials
    # Get IP/Domain
    read -rp "Is your ELK Stack reachable via HTTPS and a domain? [y/n] " REPLY_ELK_HTTPS
    if [[ "$REPLY_ELK_HTTPS" =~ ^[Yy]$ ]]; then
        REPLY_ELK_HTTPS=1
        read -rp "Please enter the DOMAIN of your ELK Stack: " REPLY_ELK_DOMAIN
        # Check if reply is a valid domain (eg. example.com or sub.example.com)
        if [[ ! "$REPLY_ELK_DOMAIN" =~ (^[A-Za-z0-9._%+-]*\.*[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$) ]]; then
            echo "This is no valid domain or subdomain. Exiting..."
            exit
        fi
    elif [[ "$REPLY_ELK_HTTPS" =~ ^[Nn]$ ]]; then
        REPLY_ELK_HTTPS=0
        read -rp "Please enter the IP/DOMAIN of your ELK Stack: " REPLY_ELK_DOMAIN
        # Check if reply is a valid domain or IPv4 (eg. 129.178.12.1 or example.com or sub.example.com)
        if ! [[ "$REPLY_ELK_DOMAIN" =~ (^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$) || "$REPLY_ELK_DOMAIN" =~ (^[A-Za-z0-9._%+-]*\.*[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$) ]]; then
            echo "This is no valid domain or IP. Exiting..."
            exit
        fi
    else
        beatsmenu
    fi

    # Get Ports
    read -rp "Please enter the PORT of your ElasticSearch instance: [9200] " ELASTIC_PORT
    ELASTIC_PORT=${ELASTIC_PORT:-9200}
    read -rp "Please enter the PORT of your Kibana instance: [5601] " KIBANA_PORT
    KIBANA_PORT=${KIBANA_PORT:-5601}
    read -rp "Please enter the PORT of your Logstash Beats Input: [5044] " LOGSTASH_PORT
    LOGSTASH_PORT=${LOGSTASH_PORT:-5044}

    # Get Credentials
    read -rp "Do you have authentication in place for ElasticSearch? [y/n] " REPLY_ELK_AUTH
        if [[ "$REPLY_ELK_AUTH" =~ ^[Yy]$ ]]; then
            REPLY_ELK_AUTH=1
            read -rp "Please enter the username for the elastic user: [elastic] " ELASTIC_USERNAME
            ELASTIC_USERNAME=${ELASTIC_USERNAME:-elastic}
            read -rsp "Please enter the password for the elastic user: " ELASTIC_PASSWORD
        elif [[ "$REPLY_ELK_AUTH" =~ ^[Nn]$ ]]; then
            REPLY_ELK_AUTH=0
        else
            beatsmenu
        fi

    # Copy templates to correct configuration file locations
    sudo cp "$SCRIPT_PATH"/beats/filebeat_files/filebeat.yml /opt/filebeat/
    sudo cp "$SCRIPT_PATH"/beats/metricbeat_files/metricbeat.yml /opt/metricbeat/
    sudo cp "$SCRIPT_PATH"/beats/auditbeat_files/auditbeat.yml /opt/auditbeat/

    # Define ELK Hostname
    sudo sed -i "s/elasticip/$REPLY_ELK_DOMAIN/g" /opt/filebeat/filebeat.yml /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    # Define Ports
    sudo sed -i "s/elasticport/$ELASTIC_PORT/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    sudo sed -i "s/kibanaport/$KIBANA_PORT/g" /opt/filebeat/filebeat.yml /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    sudo sed -i "s/logstashport/$LOGSTASH_PORT/g" /opt/filebeat/filebeat.yml
    # Enable HTTPS
    if [ "$REPLY_ELK_HTTPS" ]; then
        sudo sed -i 's/#protocol: "https"/protocol: "https"/g' /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i 's?host: "http://?host: "https://?g' /opt/filebeat/filebeat.yml /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i 's/#ssl.certificate_authorities:/ssl.certificate_authorities:/g' /opt/filebeat/filebeat.yml
        sudo sed -i 's/#ssl.certificate:/ssl.certificate:/g' /opt/filebeat/filebeat.yml
        sudo sed -i 's/#ssl.key:/ssl.key:/g' /opt/filebeat/filebeat.yml
    fi
    # Setup credentials
    if [ "$REPLY_ELK_AUTH" ]; then      
        sudo sed -i "s/#username: \"elastic\"/username: \"$ELASTIC_USERNAME\"/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i "s/#password: \"changeme\"/password: \"$ELASTIC_PASSWORD\"/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    fi
    echo ""

    sudo systemctl restart filebeat.service metricbeat.service auditbeat.service
}

function beatsConfigHeadless {
# Configure IP and PORT for ELK connection headlessly
    # Copy templates to correct configuration file locations
    sudo cp "$SCRIPT_PATH"/beats/filebeat_files/filebeat.yml /opt/filebeat/
    sudo cp "$SCRIPT_PATH"/beats/metricbeat_files/metricbeat.yml /opt/metricbeat/
    sudo cp "$SCRIPT_PATH"/beats/auditbeat_files/auditbeat.yml /opt/auditbeat/

    # Enable HTTPS
    if [ "$1" == "true" ]; then
        sudo sed -i 's/#protocol: "https"/protocol: "https"/g' /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i 's?host: "http://?host: "https://?g' /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i 's/#ssl.certificate_authorities:/ssl.certificate_authorities:/g' /opt/filebeat/filebeat.yml
        sudo sed -i 's/#ssl.certificate:/ssl.certificate:/g' /opt/filebeat/filebeat.yml
        sudo sed -i 's/#ssl.key:/ssl.key:/g' /opt/filebeat/filebeat.yml
    fi
    # Setup credentials
    if [ "$2" == "true" ]; then      
        sudo sed -i "s/#username: \"elastic\"/username: \"$7\"/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
        sudo sed -i "s/#password: \"changeme\"/password: \"$8\"/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    fi
    # Define ELK Hostname
    REPLY_ELK_DOMAIN=$3
    sudo sed -i "s/elasticip/$REPLY_ELK_DOMAIN/g" /opt/filebeat/filebeat.yml /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    # Define Ports
    ELASTIC_PORT=$4
    KIBANA_PORT=$5
    LOGSTASH_PORT=$6
    sudo sed -i "s/elasticport/$ELASTIC_PORT/g" /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    sudo sed -i "s/kibanaport/$KIBANA_PORT/g" /opt/filebeat/filebeat.yml /opt/metricbeat/metricbeat.yml /opt/auditbeat/auditbeat.yml
    sudo sed -i "s/logstashport/$LOGSTASH_PORT/g" /opt/filebeat/filebeat.yml

    sudo systemctl restart filebeat.service metricbeat.service auditbeat.service
}

function installDashboards {
#Install dashboards for beats
    cd /opt/filebeat/ || exit
    sudo ./filebeat setup --dashboards

    cd /opt/metricbeat/ || exit
    sudo ./metricbeat setup --dashboards

    cd /opt/auditbeat || exit
    sudo ./auditbeat setup --dashboards
}

function startBeats {
#Start beats
    sudo systemctl start filebeat.service metricbeat.service auditbeat.service

    SERVICE=$(sudo systemctl is-active filebeat.service auditbeat.service metricbeat.service)
    for STATUS in $SERVICE
    do
        if [[ ${STATUS} != 'active' ]]; then
            echo "[Beats] Services failed to start"
            exit
        fi
    done
    echo "[Beats] Services started successfully"

}

function stopBeats {
#Stop beats
    sudo systemctl stop filebeat.service metricbeat.service auditbeat.service

    SERVICE=$(sudo systemctl is-active filebeat.service auditbeat.service metricbeat.service)
    for STATUS in $SERVICE
    do
        if [[ ${STATUS} != 'inactive' ]]; then
            echo "[Beats] Services failed to stop"
            exit
        fi
    done
    echo "[Beats] Services stopped successfully"
}

function updateBeats {
#Updating beats
    stopBeats
    sudo rm -r /opt/filebeat /opt/metricbeat /opt/auditbeat /lib/systemd/system/filebeat.service /lib/systemd/system/auditbeat.service /lib/systemd/system/metricbeat.service
    sudo systemctl daemon-reload
    "$SCRIPT_PATH"/beats/installBeats.sh "$1"
}

function beatsmenu {
    clear
    echo ""
    echo -e "${CGREEN}Beatsmenu${CEND}"
    echo ""
    echo "What do you want to do?"
    echo "   1) Installation"
    echo "   2) Configure connection to ELK stack"
    echo "   3) Install Dashboards to Kibana"
    echo "   4) Start beats"
    echo "   5) Stop beats"
    echo "   6) Update beats"
    echo "   7) Exit"
    echo ""

    while [[ $FOPTION != "1" && $FOPTION != "2" && $FOPTION != "3" && $FOPTION != "4" && $FOPTION != "5" && $FOPTION != "6" && $FOPTION != "7" ]]; 
    do
        read -rp "Select an option [1-7]: " FOPTION
    done

    case $FOPTION in
        1) # Start beats
            read -rp "Enter your beats version [7.6.0]: " BEAT_VERSION
            installBeats "$BEAT_VERSION"
        exit
        ;;
        
        2) # Configure beats
            configBeats
        exit
        ;;
        
        3) # Install dashboards
            installDashboards
        exit
        ;;

        4) # Start beats
            startBeats
        exit
        ;;

        5) # Stop beats
            stopBeats
        exit
        ;;

        7) #Update beats
            read -rp "Enter your beats version [7.5]: " BEAT_VERSION
            updateBeats "$BEAT_VERSION"
        exit
        ;;
        
        6) #Exit Menu
            exit
        exit
        ;;
    esac
}
###################################################################################

# Code when run headless ##########################################################
if [ "$1" == "headless" ]; then
    # Switch-case to the desired functionality
    case $2 in
        "start") # Start services headlessly
            if [ "$3" == "all" ]; then
                startAllServices
                echo "[all] Services started successfully"
            elif [ "$3" == "mosquitto" ] || [ "$3" == "mariadb" ] || [ "$3" == "snmpd" ] || [ "$3" == "openssh" ]; then
                if ! startService "$3"; then
                    echo "[$3] Service failed to start"
                else
                    echo "[$3] Service started successfully"
                fi
            else
                exit
            fi
        exit
        ;;
        "stop") # Stop services headlessly
            if [ "$3" == "all" ]; then
                stopAllServices
                echo "[all] Services stopped successfully"
            elif [ "$3" == "mosquitto" ] || [ "$3" == "mariadb" ] || [ "$3" == "snmpd" ] || [ "$3" == "openssh" ]; then
                if ! stopService "$3"; then
                    echo "[$3] Service failed to stop"
                else
                    echo "[$3] Service stopped successfully"
                fi
            else
                exit
            fi
        exit
        ;;
        "reset") # Reset services headlessly
            if [ "$3" == "all" ]; then
                resetAllServices
                echo "[all] Services reset successfully"
            elif [ "$3" == "mosquitto" ] || [ "$3" == "mariadb" ] || [ "$3" == "snmpd" ] || [ "$3" == "openssh" ]; then
                if ! resetService "$3"; then
                    echo "[$3] Service failed to reset"
                else
                    echo "[$3] Service reset successfully"
                fi
            else
                exit
            fi
        exit
        ;;
        "update") # Reset services headlessly
            if [ "$3" == "all" ]; then
                updateAllServices
                echo "[all] Services updated successfully"
            elif [ "$3" == "mosquitto" ] || [ "$3" == "mariadb" ] || [ "$3" == "snmpd" ] || [ "$3" == "openssh" ]; then
                if ! updateService "$3"; then
                    echo "[$3] Service failed to update"
                else
                    echo "[$3] Service updated successfully"
                fi
            else
                exit
            fi
        exit
        ;;
        "healthcheck") # Check health of a service
            if [ "$3" == "all" ]; then
                healthcheck mariadb
                healthcheck mosquitto
                healthcheck snmpd
                healthcheck openssh
            elif [ "$3" == "mosquitto" ] || [ "$3" == "mariadb" ] || [ "$3" == "snmpd" ] || [ "$3" == "openssh" ]; then
                healthcheck "$3"
            else
                exit
            fi
        exit
        ;;
        "beats")
            if [ "$3" == "install" ]; then
                installBeats "$4"
            elif [ "$3" == "config" ]; then
                beatsConfigHeadless "$4" "$5" "$6" "$7" "$8" "$9" "${10}" "${11}"
            elif [ "$3" == "dashboards" ]; then
                installDashboards
            elif [ "$3" == "start" ]; then
                startBeats
            elif [ "$3" == "stop" ]; then
                stopBeats
            elif [ "$3" == "update" ]; then
                updateBeats "$4"
            else
                exit
            fi
        exit
    esac
    exit
fi
###################################################################################

# Menu when run directly ##########################################################
clear
echo ""
echo -e "${CGREEN}HoneyBox Installation Script!${CEND}"
echo ""
echo "What do you want to do?"
echo "   1) Prepare system for installation"
echo "   2) Install Docker rootlessly for user '$(whoami)'"
echo "   3) Start all Honeybox services (mosquitto, MariaDB+phpMyAdmin, snmpd, openSSH)"
echo "   4) Stop all Honeybox services"
echo "   5) Reset all Honeybox services"
echo "   6) Configure and start beats"
echo "   7) Exit"
echo ""

while [[ $OPTION !=  "1" && $OPTION !=  "2" && $OPTION !=  "3" && $OPTION !=  "4" && $OPTION !=  "5" && $OPTION !=  "6" && $OPTION !=  "7" ]]; 
do
	read -rp "Select an option [1-7]: " OPTION
done
###################################################################################

case $OPTION in
	1) # Install Docker rootless via precompiled binaries
        # Check if root for installation
		if [[ "$EUID" -ne 0 ]] ; then
			echo -e "${CRED}Sorry, for the system setup you need to run this step as root/sudo${CEND}"
			exit 1
        fi
        # Run installation steps
        # Change password & install required packages
        timedatectl set-timezone Europe/Berlin
        echo -e "${CGREEN}Please enter the new root password! A generated one can be seen below:${CEND}"
        strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 30 | tr -d '\n'; echo
        sudo passwd
        apt update
        apt install uidmap python3-pip libffi-dev snort unzip git make build-essential manpages-dev golang-go -y -q
        # Install mqtt client and set cronjobs to provide mqtt fake data
        wget https://github.com/hivemq/mqtt-cli/releases/download/v1.1.2/mqtt-cli_1.1.2_all.deb
        apt install ./mqtt-cli_1.1.2_all.deb -y
        rm mqtt-cli_1.1.2_all.deb
        sudo cp "$SCRIPT_PATH"/services/mosquitto/Mqtt_fake_data.sh /usr/bin/
        crontab -l > mycron
        echo "*/5 * * * * /usr/bin/Mqtt_fake_data.sh" >> mycron
        echo "@daily /home/ubuntu/Update.sh" >> mycron
        crontab mycron
        rm mycron
        # Add Snort configuration files
        systemctl stop snort
        systemctl disable snort
        mv firewall/snort.conf /etc/snort
        mv firewall/*.rules /etc/snort/rules
        rm -rf firewall/
        # Install docker-compose
        pip3 install docker-compose
        chown -R ubuntu:ubuntu /home/ubuntu/bin/
        chmod u+x /home/ubuntu/bin/*
        # Change SSH port and disallow root access
        sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
        sed -i "s/#Port 22/Port 2122/g" /etc/ssh/sshd_config
        # Allow users to open sockets with well-known ports
        echo "net.ipv4.ip_unprivileged_port_start=0" >> /etc/sysctl.conf
        # Global env variables for Docker 
        echo "DOCKER_HOST=unix:///run/user/docker.sock" >> /etc/environment
        echo "COMPOSE_HTTP_TIMEOUT=600" >> /etc/environment
        # Add systemd service for userspace mounts
        cat <<EOM > /etc/systemd/system/userspacemounts.service
[Unit]
Description=Enable mounts in userspace for Docker rootless
Before=docker.service

[Service]
ExecStart=/usr/sbin/modprobe overlay permit_mounts_in_userns=1 aufs br_netfilter

[Install]
WantedBy=multi-user.target
EOM
        systemctl enable userspacemounts.service
        modprobe overlay permit_mounts_in_userns=1
        # Add systemd regenerate-SSH-Hostkeys file
        cat <<EOM > /lib/systemd/system/regenerate_ssh_host_keys.service
[Unit]
Description=Regenerate SSH host keys
Before=ssh.service

[Service]
Type=oneshot
ExecStartPre=-/bin/sh -c "/bin/rm -f -v /etc/ssh/ssh_host_*_key*"
ExecStart=/usr/bin/ssh-keygen -A -v
ExecStartPost=/bin/systemctl disable regenerate_ssh_host_keys

[Install]
WantedBy=multi-user.target
EOM
        cat <<EOM > /usr/bin/DockerDelayFix.sh
#!/bin/bash        
sudo service docker restart
sleep 5
sudo modprobe overlay permit_mounts_in_userns=1
sleep 5
sudo service docker restart
sleep 5
EOM
        # Install Docker Compose via pip
        echo -e "${CGREEN}Finished installing prerequisites. Installing Docker Compose now...${CEND}"
        pip3 install -q docker-compose
        echo -e "${CGREEN}Finished installing docker-compose. Exiting now...${CEND}"
        echo ""
        read -rp "Do you want to reboot now? [y/n] " REPLY_REBOOT
	    if [[ "$REPLY_REBOOT" =~ ^[Yy]$ ]]; then
            echo "Remember that your SSH port has been changed to 2122."
            sleep 5
            reboot
        fi
	exit
    ;;

	2) # Install Docker rootlessly
		# Check if non-root, else exit
		if [[ "$EUID" -eq 0 ]]; then
			echo -e "${CRED}Sorry, you need to run this script as a non root user.${CEND}"
			exit 1
		fi
        mkdir /home/ubuntu/ca/
        # Download and unpack official Docker binaries to /home/ubuntu/bin
        wget https://download.docker.com/linux/static/stable/armhf/docker-"$DOCKER_VERSION".tgz -O docker-rootless.tgz
        wget https://download.docker.com/linux/static/stable/armhf/docker-rootless-extras-"$DOCKER_VERSION".tgz -O docker-rootless-extras.tgz
        tar xfvz docker-rootless.tgz -C /home/ubuntu/bin --strip 1 --overwrite
        tar xfvz docker-rootless-extras.tgz -C /home/ubuntu/bin --strip 1
        chmod u+x /home/ubuntu/bin/*
        rm docker-*.tgz
        # Add systemwide systemd startup service for Docker
        sudo cat <<EOM | sudo tee /etc/systemd/system/docker.service
[Unit]
Description=Docker Application Container Engine (Rootless)
Documentation=https://docs.docker.com

[Service]
Environment=PATH=/home/ubuntu/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
Environment=DOCKER_HOST=unix:///run/user/docker.sock
Environment=COMPOSE_HTTP_TIMEOUT=600
Environment=XDG_RUNTIME_DIR=/run/user/
User=ubuntu
Group=ubuntu
RuntimeDirectory=user
RuntimeDirectoryMode=0777
ExecStart=/home/ubuntu/bin/dockerd-rootless.sh --experimental --storage-driver=overlay2
ExecReload=/bin/kill -s HUP \$MAINPID
TimeoutSec=0
RestartSec=5
Restart=always
StartLimitBurst=3
StartLimitInterval=60s
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
Delegate=yes
Type=simple

[Install]
WantedBy=multi-user.target
EOM
    sudo systemd daemon-reload
    sudo systemctl enable docker
    sudo systemctl start docker
	exit
	;;
	
	3) # Deploy vulnerable Docker services
        # MQTT - mosquitto
        echo ""
        echo -e "${CYELLOW}Deploying the MQTT service...${CEND}"
        if [ "$(docker inspect -f "{{.State.Running}}" mosquitto 2>/dev/null)" ]; then
            stopService mosquitto
        fi
        #buildImage mosquitto
        cd "$SCRIPT_PATH"/services/mosquitto || exit
        if [ ! -d "$SCRIPT_PATH"/services/mosquitto/log ]; then
            mkdir "$SCRIPT_PATH"/services/mosquitto/log
        fi
        startService mosquitto

        # SQL - MariaDB & phpMyAdmin
        echo -e "${CYELLOW}Deploying the SQL services...${CEND}"
        if [ "$(docker inspect -f "{{.State.Running}}" mariadb 2>/dev/null)" ]; then
            stopService mariadb
        fi
        #buildImage mariadb
        cd "$SCRIPT_PATH"/services/mariadb || exit
        if [ ! -d "$SCRIPT_PATH"/services/mariadb/data ]; then
            mkdir "$SCRIPT_PATH"/services/mariadb/data
        fi
        startService mariadb

        # SNMP - snmpd
        echo -e "${CYELLOW}Deploying the SNMP service...${CEND}"
        if [ "$(docker inspect -f "{{.State.Running}}" snmpd 2>/dev/null)" ]; then
            stopService snmpd
        fi
        #buildImage snmpd
        cd "$SCRIPT_PATH"/services/snmpd || exit
        if [ ! -d "$SCRIPT_PATH"/services/snmpd/log ]; then
            mkdir "$SCRIPT_PATH"/services/snmpd/log
        fi
        startService snmpd

        # SSH - openSSH
        echo -e "${CYELLOW}Deploying the SSH service...${CEND}"
        if [ "$(docker inspect -f "{{.State.Running}}" openssh 2>/dev/null)" ]; then
            stopService openssh
        fi
        #buildImage openssh
        cd "$SCRIPT_PATH"/services/openssh || exit
        if [ ! -d "$SCRIPT_PATH"/services/openssh/log ]; then
            mkdir "$SCRIPT_PATH"/services/openssh/log
        fi
        startService openssh

        echo ""
        echo -e "${CGREEN}Your services should be up and running!${CEND}"
        echo ""
        echo -e "   1) MQTT should be running at mqtt://$HOST_IP:1883 ..."
        echo -e "   2) SQL should be running at http://$HOST_IP:3306 ..."
        echo -e "   3) phpMyAdmin should be running at http://$HOST_IP:80 ..."
        echo -e "   4) SNMP should be running at http://$HOST_IP:161 ..."
        echo -e "   5) SSH should be running at ssh://$HOST_IP:22 ..."
        echo ""
    exit
    ;;

    4) # Stop all services
        echo ""
        stopAllServices
        echo -e "${CGREEN}All services have been stopped!${CEND}"
        echo ""
	exit
    ;;

    5) # Reset all services
        echo ""
        resetAllServices
        echo -e "${CGREEN}All services have been reset!${CEND}"
        echo ""
	exit
    ;;

    6) # Configure and install beats
        beatsmenu
    exit
    ;;

    7) # Exit script
        exit
	exit
	;;
esac
