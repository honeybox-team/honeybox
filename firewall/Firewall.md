# UFW Firewall configuration

Install: `apt install ufw`

### SSH only from trusted network
```
ufw allow from 192.168.0.0/24 to any port 22 proto tcp
```

--> for script:
`ufw allow from X.X.X.X/YY to any port XYZ proto tcp`


### basic setup
```
ufw allow http
ufw allow https
ufw allow ntp # we might can remove this or set only out
ufw allow dns
```

-->

```
To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       192.168.0.0/24
80/tcp                     ALLOW       Anywhere
443/tcp                    ALLOW       Anywhere
123/udp                    ALLOW       Anywhere
DNS                        ALLOW       Anywhere
80/tcp (v6)                ALLOW       Anywhere (v6)
443/tcp (v6)               ALLOW       Anywhere (v6)
123/udp (v6)               ALLOW       Anywhere (v6)
DNS (v6)                   ALLOW       Anywhere (v6)
```

### /etc/default/ufw

Change to:

```
# /etc/default/ufw
#

IPV6=yes

DEFAULT_INPUT_POLICY="REJECT"

DEFAULT_OUTPUT_POLICY="ACCEPT"

DEFAULT_FORWARD_POLICY="REJECT"

DEFAULT_APPLICATION_POLICY="SKIP"
```
Would be great it we could also set OUTPUT to REJECT,
however if we do this stuff like apt, dns, etc. will not work anymore.

TODO: make it work with "REJECT" on output --> need more time and manpower! 

Services from Docker are higher inside the IPTables list and are therefore automatically exposed --> we do not have to allow them.
--> checked with kali