# Honeybox

## InstallHoneybox.sh | [![pipeline status](https://gitlab.com/honeybox-team/honeybox/badges/master/pipeline.svg)](https://gitlab.com/honeybox-team/honeybox/commits/master)

This is the current installation and management script for the Honeybox project. It provides manual script routines...
- to initially prepare a freshly installed Ubuntu 19.10 ARM an a Raspberry Pi.
    - An in-depth guide can be found in the [Wiki](https://gitlab.com/honeybox-team/honeybox/-/wikis/Guides/Image%20creation)
- to configure and install a rootless variant of the Docker Community Edition
- to install and setup ARM versions of Elastic's File-, Metric- and Auditbeat shippers.
- to setup the vulnerable services (MariaDB+phpMyAdmin, mosquitto, openSSH, snmpd) on said Docker stack.
- to manage the services as a whole (Start, Stop, Reset).

To enable remote functionality via the webinterface, a headless API has been implemented into the script, which provides...
- granular service management (Start, Stop, Reset, Update, Healthcheck single services)
- installation and configuration of the beat services
- mangement of the beat services

![Menu](https://theforcer.de/honeybox/menu.png)

### Usage (CLI)

```sh
chmod u+x InstallHoneybox.sh
./InstallHoneybox.sh
```

### Usage (headless)

```sh
chmod u+x InstallHoneybox.sh
./InstallHoneybox.sh headless <start|stop|reset|update|healthcheck> <all|mosquitto|mariadb|snmpd|openssh>
```
The headless mode allows you to control the script without using the CLI, so that it's functionality can be easily implemented for automation (e.g cronjobs) or remote execution via SSH (as required for our webinterface.)

`./InstallHoneybox.sh headless start mosquitto` -> (Re)Start the mosquitto service

`./InstallHoneybox.sh headless stop all` -> Shutdown all services

`./InstallHoneybox.sh headless reset openssh` -> Stops service, deletes local data and starts it up fresh

`./InstallHoneybox.sh headless update snmpd` -> Pulls the latest image from our DockerHub repository and restarts the service

`./InstallHoneybox.sh headless healthcheck openssh` -> Echoes "stopped" or "running" according to service state

## Update.sh | [![pipeline status](https://gitlab.com/honeybox-team/honeybox/badges/master/pipeline.svg)](https://gitlab.com/honeybox-team/honeybox/commits/master)

This is a simple bash script used to automate package updates of the Honeybox system. When used in a cron job for example, it will search for available package update, install those and remove any orphaned/leftover packages as well. Output of all these commands will be written into a logfile, as defined by the LOG variable in the script. All error output will be written to the logfile LOG_ERROR. The headless implementation enables a simple interface to get the latest update data.

### Usage (e.g. cron)

```sh
chmod +x Update.sh
<put the following line in the root users cronfile>
@daily /home/ubuntu/Update.sh
```

### Usage (headless)

```sh
chmod +x Update.sh
./Update.sh headless stats
```

The headless command returns a list in the form of `[Sun 24 Nov 08:18:42 GMT 2019, 0 upgraded, 0 newly installed, 0 to remove, 0 not upgraded]` via STDOUT or `No Updates done yet!`, if the LOG is empty. First entry is the timestamp of the latest update, the other ones the amount of packages which have been upgraded etc. Should one of the internal update functions not execute correctly, the command will print the errors of the respective command via STDERR. 
