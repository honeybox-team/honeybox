#!/bin/bash
set -e

echo "[Entrypoint] Team Honeybox snmpd Docker Image"
# Check if the log file already exists, otherwise create it
if [ ! -n "$(ls -A /var/log/snmpd/)"  ]; then
    echo "[Entrypoint] Logfile does not exist, creating one now..."
    touch /var/log/snmpd/snmpd.log
fi

echo "[Entrypoint] Starting snmpd"
exec "$@"