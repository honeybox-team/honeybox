#!/bin/bash

mqtt pub -i xyz321 -t SmartHome -m '{"Device": "Stereo Living Room", "Status": "OFF", "Title": "Nothing is playing"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SmartHome -m '{"Device": "TV Living Room", "Status": "ON", "Channel": "AMC", "Show": "The Walking Dead"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SmartHome -m '{"Device": "Lamp Living Room", "Status": "ON", "Color": "#FFFFFF"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SmartHome -m '{"Device": "TV Bedroom", "Status": "OFF", "Channel": "No Channel", "Show": "No Show"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SmartHome -m '{"Device": "Lamp Bedroom", "Status": "OFF", "Color": "FF0000"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SmartHome -m '{"Device": "Main Server", "Status": "ON", "Disk Space": "120GB/1TB"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SecurityDevices -m '{"Device": "Camera Front Door", "Status": "ON"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SecurityDevices -m '{"Device": "Camera Garden", "Status": "ON"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SecurityDevices -m '{"Device": "Door Garage", "Status": "Closed"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SecurityDevices -m '{"Device": "Door Front", "Status": "Closed"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t SecurityDevices -m '{"Device": "Door Garden", "Status": "Opened"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t FamilyLocations -m '{"Person": "Rick", "Location": "At Work", "Address": "1600 Amphitheatre Pkwy, Mountain View, CA 94043, Vereinigte Staaten"}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t FamilyLocations -m '{"Person": "Lois", "Location": "At Home", "Address": "3195 Falls Creek Dr, San Jose, CA 95135, USA "}' -V 3 >/dev/null 2>&1
mqtt pub -i xyz321 -t FamilyLocations -m '{"Person": "Carl", "Location": "At Home", "Address": "3195 Falls Creek Dr, San Jose, CA 95135, USA "}' -V 3 >/dev/null 2>&1

