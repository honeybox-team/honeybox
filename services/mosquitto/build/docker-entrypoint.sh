#!/bin/ash
set -e

echo "[Entrypoint] mosquitto Docker Image"
# Check if the log file already exists, otherwise create it
if [ ! -n "$(ls -A /mosquitto/log/)"  ]; then
    echo "[Entrypoint] Logfile does not exist, creating one now..."
    touch /mosquitto/log/mosquitto.log
    chown mosquitto:mosquitto /mosquitto/log/mosquitto.log
    chmod 606 /mosquitto/log/mosquitto.log
fi

echo "[Entrypoint] Starting mosquitto"
exec "$@"