#!/bin/bash
set -e

echo "[Entrypoint] Team Honeybox OpenSSH Docker Image"
# Check if the log file already exists, otherwise create it
if [ ! -n "$(ls -A /var/private/honeynet/login-attempts/)"  ]; then
    echo "[Entrypoint] Logfile does not exist, creating one now..."
    touch /var/private/honeynet/login-attempts/openssh.log
fi

echo "[Entrypoint] Starting OpenSSH"
exec "$@"
