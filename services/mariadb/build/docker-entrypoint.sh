#!/bin/ash
set -e

echo "[Entrypoint] MariaDB Docker Image"
# Check if the data directory already exists, otherwise initialize DB
if [ ! -n "$(ls -A /var/lib/mysql)"  ]; then
    echo "[Entrypoint] DATADIR does not exist, creating it now..."
    mkdir -p "/var/lib/mysql"
    chown -R mariadb:mariadb "/var/lib/mysql"

    echo "[Entrypoint] Initializing database"
    /usr/bin/mysql_install_db --user=mariadb --datadir=/var/lib/mysql
    echo '[Entrypoint] Database initialized'

    echo '[Entrypoint] Starting service and adding allowed root user'
    "$@" &
    sleep 2
    mysql -u root -e "CREATE USER 'root'@'%' IDENTIFIED BY '';"
    mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '' WITH GRANT OPTION; REVOKE DROP, DELETE ON *.* FROM 'root'@'%'; FLUSH PRIVILEGES;"        

    echo '[Entrypoint] Creating and importing mock SQL data'
    mysql -u root -e "CREATE DATABASE articles; CREATE DATABASE customers; CREATE DATABASE passwords; CREATE DATABASE position; CREATE DATABASE salaries; CREATE DATABASE shopping_cart; CREATE DATABASE users;"
    mysql -u root articles < /var/sql/articles.sql
    mysql -u root customers < /var/sql/customers.sql
    mysql -u root passwords < /var/sql/passwords.sql
    mysql -u root position < /var/sql/position.sql
    mysql -u root salaries < /var/sql/salaries.sql
    mysql -u root shopping_cart < /var/sql/shopping_cart.sql
    mysql -u root users < /var/sql/users.sql
    #chown -R mariadb:mariadb /var/lib/mysql
    chmod -R 707 /var/lib/mysql
    rm -r /var/sql/

    echo '[Entrypoint] Shutdown service again'
    mysqladmin -u root shutdown
fi

echo "[Entrypoint] Starting MariaDB"
exec "$@"