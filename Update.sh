#!/bin/bash

# Variables (Create files if non-existent) ########################################
LOG="/var/log/backup.log"
if ! [ -f "$LOG" ]; then
    touch "$LOG"
fi
LOG_ERROR="/var/log/backup_error.log"
if ! [ -f "$LOG_ERROR" ]; then
    touch "$LOG_ERROR"
fi
###################################################################################

# Code when run headless ##########################################################
if [ "$1" == "headless" ]; then
    # Switch-case to the desired functionality
    case $2 in
        "stats") # Return stats from most recent update, if 
                 #  - there was no error updating
                 #  - the log file is not empty
            if ! (grep -E "^E\:|^W\:" $LOG) > /dev/null && [ -s "$LOG" ]; then
                DATE=$(head -1 "$LOG")
                UPGRADED=$(grep -Eo "([0-9]+ upgraded)" "$LOG" | head -1)
                INSTALLED=$(grep -Eo "([0-9]+ newly installed)" "$LOG" | head -1)
                REMOVED=$(grep -Eo "([0-9]+ to remove)" "$LOG" | head -1)
                NOT_UPGRADED=$(grep -Eo "([0-9]+ not upgraded)" "$LOG" | head -1)
                echo "[$DATE, $UPGRADED, $INSTALLED, $REMOVED, $NOT_UPGRADED]"
            elif ! [ -s "$LOG" ]; then
                echo "No Updates done yet!"
            else
                >&2 cat $LOG_ERROR
                exit
            fi
        exit
        ;;
    esac
fi
###################################################################################

# Code when run directly ##########################################################
# Write stdout/stderr to different log files
exec > >(tee -i ${LOG})
exec 2> >(tee -i ${LOG_ERROR})

# Start updating
date
echo ""
echo "###### Starting apt update on $(date) ######"
if ! apt-get update; then
    echo "[WARNING]: apt update failed"
    exit
fi
echo ""
# Start upgrading
echo "###### Starting apt upgrade on $(date) ######"
if ! apt-get -y full-upgrade; then
    echo "[WARNING]: apt upgrade failed"
    exit
fi
echo ""
# Start autoclean
echo "###### Starting apt autoremove on $(date) ######"
apt-get -y autoremove
echo ""
###################################################################################