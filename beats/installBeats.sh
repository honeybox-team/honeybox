#!/bin/bash

# Script variables
SCRIPT_PATH="$(pwd)"
BEAT_VERSION=$1
BEAT_VERSION=${BEAT_VERSION:-7.6.0}

echo "Creating Go Workspace directories..." 
if [ -d "$HOME"/go ];
then
   echo " -> $HOME/go already exists" 
else
   echo " -> making $HOME/go..."  
   export GOPATH=$HOME/go
   go get github.com/elastic/beats 
fi
echo " -> Go Workspace directories created" 

echo "Getting Beats files from Elastic repo on github..." 
cd "$HOME"/go/src/github.com/elastic/beats/ || exit
echo "Checking out Beats..." 
git fetch
git checkout "${BEAT_VERSION:0:3}"


echo "---- Filebeat ----"
cd "$HOME"/go/src/github.com/elastic/beats/filebeat || exit
if [ "$(lscpu | grep -c "armv7")" -eq 1 ];
then
  echo "Creating Filebeat..."  
  GOOS=linux GOARCH=arm go get;
  make;
  echo "Filebeat created"  
elif [ "$(lscpu | grep -c "armv8")" -eq 1 ];
then 
  echo "Creating Filebeat..."  
  GOOS=linux GOARCH=arm64 go get;
  make;
  echo "Filebeat created"  
fi

echo "Moving Filebeat..." 


echo "---- Metricbeat ----"
cd "$HOME"/go/src/github.com/elastic/beats/metricbeat || exit
if [ "$(lscpu | grep -c "armv7")" -eq 1 ];
then
  echo "Creating Metricbeat..."  
  GOOS=linux GOARCH=arm go get;
  make;
  echo "Metricbeat created"  
elif [ "$(lscpu | grep -c "armv8")" -eq 1 ];
then 
  echo "Creating Metricbeat..."  
  GOOS=linux GOARCH=arm64 go get;
  make;
  echo "Metricbeat created"  
fi


echo "---- Auditbeat ----"
cd "$HOME"/go/src/github.com/elastic/beats/auditbeat || exit
if [ "$(lscpu | grep -c "armv7")" -eq 1 ];
then
  echo "Creating Auditbeat..."  
  GOOS=linux GOARCH=arm go get;
  make;
  echo "Auditbeat created"  
elif [ "$(lscpu | grep -c "armv8")" -eq 1 ];
then 
  echo "Creating Auditbeat..."  
  GOOS=linux GOARCH=arm64 go get;
  make;
  echo "Auditbeat created"  
fi

echo "Moving beats int correct /opt/ directory"
sudo rm -r /opt/filebeat
sudo mv "$HOME"/go/src/github.com/elastic/beats/filebeat/ /opt/
sudo cp "$SCRIPT_PATH"/beats/filebeat_files/filebeat.service /lib/systemd/system
sudo /bin/systemctl daemon-reload
sudo systemctl enable filebeat
chmod go-w /opt/filebeat/filebeat.yml
sudo chown -R root:root /opt/filebeat
cd ~/ || exit
wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-"$BEAT_VERSION"-linux-x86.tar.gz -O ~/filebeat.tar.gz
sudo tar -xvzf filebeat.tar.gz 
sudo cp -r filebeat-"$BEAT_VERSION"-linux-x86/kibana/ /opt/filebeat/
sudo rm -r filebeat-"$BEAT_VERSION"-linux-x86/
sudo rm filebeat.tar.gz

sudo rm -r /opt/metricbeat
sudo mv "$HOME"/go/src/github.com/elastic/beats/metricbeat/ /opt/
sudo cp "$SCRIPT_PATH"/beats/metricbeat_files/mysql.yml /opt/metricbeat/modules.d/
sudo cp "$SCRIPT_PATH"/beats/metricbeat_files/metricbeat.service /lib/systemd/system
sudo /bin/systemctl daemon-reload
sudo systemctl enable metricbeat
chmod go-w /opt/metricbeat/modules.d/system.yml
chmod go-w /opt/metricbeat/metricbeat.yml
sudo chown -R root:root /opt/metricbeat
sudo chmod go-w /opt/metricbeat/modules.d/*
cd ~/ || exit
wget https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-"$BEAT_VERSION"-linux-x86.tar.gz -O ~/metricbeat.tar.gz
sudo tar -xvzf metricbeat.tar.gz
sudo cp -r metricbeat-"$BEAT_VERSION"-linux-x86/kibana/ /opt/metricbeat/
sudo rm -r metricbeat-"$BEAT_VERSION"-linux-x86/
sudo rm metricbeat.tar.gz

sudo rm -r /opt/auditbeat
sudo mv "$HOME"/go/src/github.com/elastic/beats/auditbeat/ /opt/
sudo cp "$SCRIPT_PATH"/beats/auditbeat_files/auditbeat.service /lib/systemd/system
sudo /bin/systemctl daemon-reload
sudo systemctl enable auditbeat
chmod go-w /opt/auditbeat/auditbeat.yml
sudo chown -R root:root /opt/auditbeat
wget https://artifacts.elastic.co/downloads/beats/auditbeat/auditbeat-"$BEAT_VERSION"-linux-x86.tar.gz -O ~/auditbeat.tar.gz
sudo tar -xvzf auditbeat.tar.gz
sudo cp -r auditbeat-"$BEAT_VERSION"-linux-x86/kibana/ /opt/auditbeat/
sudo rm -r auditbeat-"$BEAT_VERSION"-linux-x86/
sudo rm auditbeat.tar.gz

echo " Deleting Go directory"
rm -rf "$HOME"/go

echo "Finished" 
echo "---------------------------------------------------------------"
