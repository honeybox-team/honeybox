# Filebeat (access and testing)

## Logstash
Logstash itselfe will follow soon when everything works more or less.

## Server information
For testing reasons there is a server running logstash, elasticsearch and kibana.
You may connect to the server using **ssh -i /path/to/keyfile debian@193.197.228.135**.
The keyfile needed is stored in the ssh directory underneath this one.

## Config
You will find the logstash config file at **/etc/logstash/conf.d/logstash.conf** and the
kibana config at **/home/debian/kibana-7.4.2-linux-x86_64/config/kibana.yml**.

## Systemd
There should be systemd services for each service (kibana, logstash, elasticsearch).
- sudo systemctl [start/stop/status] kibana.service
- sudo systemctl [start/stop/status] logstash.service
- sudo systemctl [start/stop/status] elasticsearch.service

## Logs
Logs are accessible at:
- /var/log/logstash/
- /var/log/elasticsearch/

## Kibana webservice
To visite kibana just connect to **http://193.197.228.135:5601** with your favorite browser.

