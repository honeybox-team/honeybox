from flask import Flask, render_template, request, redirect, url_for, flash

app = Flask(__name__)

import datetime
import paramiko
import configparser
import requests
import subprocess
import os
import socket
import ipaddress
import string
import re


# get config values of config.ini
config = configparser.ConfigParser()
config.read("config.ini")

hostname = config["SSH"]["hostname"]
username = config["SSH"]["username"]
password = config["SSH"]["password"]
port = int(config["SSH"]["port"])
interface = config["SSH"]["interface"]
app.secret_key = config["SSH"]["secret_key"].encode()


ssh = paramiko.SSHClient()


## SSH functions


def saveExecution(cmd):
    """Returns SSH output (3-tuple with stdin, 
    stdout and stderr) of the executed command over SSH"""
    return ssh.exec_command(cmd)


def executeCmd(cmd):
    """Check for a know command and execute it
    by calling saveExecution.
    Returns SSH output as 3-tuple."""

    headlessCmd = "/home/ubuntu/InstallHoneybox.sh headless "

    if cmd == "update":
        return saveExecution("sudo /home/ubuntu/Update.sh headless")

    elif cmd == "off":
        return saveExecution("sudo poweroff")

    elif cmd == "reboot":
        return saveExecution("sudo reboot")

    elif cmd == "kernel":
        return saveExecution("uname -r")

    elif cmd == "uptime":
        return saveExecution("uptime -p")

    elif cmd == "ipv4":
        return saveExecution(
            "ip -4 addr show "
            + str(interface)
            + " | grep -oP '(?<=inet\s)\d+(\.\d+){3}'"
        )

    elif cmd == "mac":
        return saveExecution("cat /sys/class/net/" + str(interface) + "/address")

    elif cmd == "hostname":
        return saveExecution("hostname")

    elif cmd == "change-pw":
        # echo "ubuntu:honeybox2" | sudo chpasswd
        # echo "ubuntu:"$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16) | sudo chpasswd
        return saveExecution(
            "echo 'ubuntu:'$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16) | sudo chpasswd"
        )

    elif cmd == "clear-history":
        # clear bash_history
        # > ~/.bash_history && history -c
        return saveExecution("> ~/.bash_history && history -c")

    elif cmd == "disable-pw-auth":
        # TODO: no idea if PermitRootLogin is even needed --> ubuntu user
        # sudo sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
        # sudo sed -i "s/PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config
        return saveExecution(
            "sudo sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config"
        )

    # service control
    elif cmd == "start-all":
        return saveExecution(str(headlessCmd) + "start all")

    elif cmd == "stop-all":
        return saveExecution(str(headlessCmd) + "stop all")

    elif cmd == "reset-all":
        return saveExecution(str(headlessCmd) + "reset all")

    elif cmd == "update-all":
        return saveExecution(str(headlessCmd) + "update all")

    elif cmd == "start-mariadb":
        return saveExecution(str(headlessCmd) + "start mariadb")

    elif cmd == "start-mosquitto":
        return saveExecution(str(headlessCmd) + "start mosquitto")

    elif cmd == "start-snmpd":
        return saveExecution(str(headlessCmd) + "start snmpd")

    elif cmd == "start-ssh":
        return saveExecution(str(headlessCmd) + "start openssh")

    elif cmd == "stop-mariadb":
        return saveExecution(str(headlessCmd) + "stop mariadb")

    elif cmd == "stop-mosquitto":
        return saveExecution(str(headlessCmd) + "stop mosquitto")

    elif cmd == "stop-snmpd":
        return saveExecution(str(headlessCmd) + "stop snmpd")

    elif cmd == "stop-ssh":
        return saveExecution(str(headlessCmd) + "stop openssh")

    elif cmd == "reset-mariadb":
        return saveExecution(str(headlessCmd) + "reset mariadb")

    elif cmd == "reset-mosquitto":
        return saveExecution(str(headlessCmd) + "reset mosquitto")

    elif cmd == "reset-snmpd":
        return saveExecution(str(headlessCmd) + "reset snmpd")

    elif cmd == "reset-ssh":
        return saveExecution(str(headlessCmd) + "reset openssh")

    elif cmd == "update-mariadb":
        return saveExecution(str(headlessCmd) + "update mariadb")

    elif cmd == "update-mosquitto":
        return saveExecution(str(headlessCmd) + "update mosquitto")

    elif cmd == "update-snmpd":
        return saveExecution(str(headlessCmd) + "update snmpd")

    elif cmd == "update-ssh":
        return saveExecution(str(headlessCmd) + "update openssh")

    elif cmd == "check-mariadb":
        return saveExecution(str(headlessCmd) + "healthcheck mariadb")

    elif cmd == "check-mosquitto":
        return saveExecution(str(headlessCmd) + "healthcheck mosquitto")

    elif cmd == "check-snmpd":
        return saveExecution(str(headlessCmd) + "healthcheck snmpd")

    elif cmd == "check-ssh":
        return saveExecution(str(headlessCmd) + "healthcheck openssh")

    ## beats control
    elif cmd == "set-beats":
        # race condition/ TOCTOU if attacker has access to the server and writes
        # fast enough inside the config file (first writing in config file, then reading out of it)
        # then there's the possiblity of a RCE.
        # However if the attacker is already on the system, then he could also steal
        # our private key --> this flaw should be justifiable
        https = str(readConfigIni("BEATS", "usehttps"))
        auth = str(readConfigIni("BEATS", "useauth"))
        server = str(readConfigIni("BEATS", "server"))

        elasticPort = str(readConfigIni("BEATS", "elasticport"))
        kibanaPort = str(readConfigIni("BEATS", "kibanaport"))
        logstashPort = str(readConfigIni("BEATS", "logstashport"))

        elasticUser = str(readConfigIni("BEATS", "elasticuser"))
        elasticPW = str(readConfigIni("BEATS", "elasticpw"))

        if elasticPW == "elasticDefaultPW":
            elasticPW = "0"

        # ./InstallHoneybox.sh headless beats config <true/false HTTPS> <true/false Auth> <server> <elasticPort> <kibanaPort> <logstashPort> <elasticUsername> <elasticPassword>
        return saveExecution(
            str(headlessCmd)
            + "beats config "
            + https
            + " "
            + auth
            + " "
            + server
            + " "
            + elasticPort
            + " "
            + kibanaPort
            + " "
            + logstashPort
            + " "
            + elasticUser
            + " "
            + elasticPW
        )

    elif cmd == "start-beats":
        return saveExecution(str(headlessCmd) + "beats start")

    elif cmd == "stop-beats":
        return saveExecution(str(headlessCmd) + "beats stop")


def doSSH(cmdList=[], sftp="false", pushFiles={}):
    """Login on remote host with SSH. If no parameter is provided
    then only do an alive-check. Otherwise check if the 
    command/s is/are known and remap it/them using executeCmd.
    Returns SSH output as a dict with cmd:2-tuple or alive-value.
    Example: myDict = doSSH(['ls', 'hostname']) 
    --> stdouts: myDict['ls'][0] and myDict['hostname'][0]
    [0] for stdout, [1] for stderr 
    """

    outputSSH = None
    alive = False

    # reread hostname
    hostname = readConfigIni("SSH", "hostname")

    try:
        # might improve on this later
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # check if user wants to use password or public key auth
        if readConfigIni("SSH", "usePassword") == "true":
            print("Trying password")
            ssh.connect(
                hostname=hostname,
                port=port,
                username=username,
                password=password,
                timeout=2,
            )

        else:
            print("Trying key")
            ssh.connect(
                hostname=hostname,
                port=port,
                username=username,
                key_filename="honeyboxKey",
                timeout=2,
            )


        if cmdList:
            outputSSH = {}
            for cmd in cmdList:
                stdin, stdout, stderr = executeCmd(cmd)
                sshOutput = stdout.read().decode("utf-8")
                sshError = stderr.read().decode("utf-8")
                outputSSH[cmd] = [sshOutput, sshError]

        elif sftp == "true":
            sftp = ssh.open_sftp()
            for pFile in pushFiles:
                sftp.put(pushFiles[pFile][0], pushFiles[pFile][1])
            sftp.close()

        # no cmd, just check for connection/ if host is alive
        else:
            if ssh.get_transport().is_active():
                alive = True
            else:
                alive = False

    except paramiko.ssh_exception.BadAuthenticationType:
        flash("Bad SSH authentication type. Restarting the honeybox can help.")

    except paramiko.ssh_exception.SSHException:
        flash("Could not connect to your honeybox.")

    except socket.timeout:
        pass

    except Exception as e:
        flash(str(e))

    finally:
        ssh.close()
        if cmdList:
            return outputSSH
        else:
            return alive


## get JSON output from elasticsearch
def fetchElastic(queryType, beat, searchPar=None):

    useAuth = readConfigIni("BEATS", "useauth")
    elasticUser = readConfigIni("BEATS", "elasticuser")
    elasticPW = readConfigIni("BEATS", "elasticpw")

    headers = {
        "Content-Type": "application/json",
    }

    apiURL = "http://localhost:9201/" + str(beat) + "/_search"

    if str(queryType) == "search":
        data = (
            '{  "version": true,  "size": 500,  "sort": [    {      "@timestamp": {        "order": "desc",        "unmapped_type": "boolean"      }    }  ],    "_source": "message",  "query": {    "bool": {      "must": [],      "filter": [        {          "bool": {            "should": [              {                "match_phrase": {                  "message": "'
            + str(searchPar)
            + '"                }              }            ],            "minimum_should_match": 1          }        }              ]    }  }}'
        )

    elif str(queryType) == "metrics":
        data = '{"version": true,"size": 1,"sort": [    {    "@timestamp": {        "order": "desc",        "unmapped_type": "boolean"    }    }]}'
        # example usage
        # outputElastic = fetchElastic("metrics", "metric*")

    elif str(queryType) == "mysql":
        data = '{ "version": true, "size": 500, "sort": [ { "@timestamp": { "order": "desc", "unmapped_type": "boolean" } } ], "_source": ["Warning", "@timestamp"], "query": { "bool": { "should": [{ "exists": { "field": "droquery" } }, { "exists": { "field": "insquery" } }, { "exists": { "field": "updquery" } } ] } }}'

    elif str(queryType) == "mqtt":
        data = '{  "version": true,        "size": 500,        "sort": [            {            "@timestamp": {                "order": "desc",                "unmapped_type": "boolean"            }            }        ],    "_source": ["message", "@timestamp"],    "query": {        "exists": {            "field": "p2topic"        }    }}'

    elif str(queryType) == "ssh":
        data = '{  "version": true,        "size": 500,        "sort": [            {            "@timestamp": {                "order": "desc",                "unmapped_type": "boolean"            }            }        ],    "_source": ["message", "@timestamp"],    "query": {        "exists": {            "field": "loginattempt"        }    }}'

    try:
        if useAuth == "true":
            # response = requests.post('http://127.0.0.1:9201/_search', headers=headers, data=data, auth=('elastic', 'xxxxxxxxxxx'))
            response = requests.post(apiURL, headers=headers, data=data, auth=(elasticUser, elasticPW))
        else: 
            response = requests.post(apiURL, headers=headers, data=data,)
    except:
        return None
    return response.json()


## write entry inside the config.ini
def writeConfigIni(category, key, value):
    with open("config.ini", "w") as configfile:
        config[category][key] = value
        config.write(configfile)


## read entry from config.ini
def readConfigIni(category, key):
    config.read("config.ini")
    value = config[category][key]
    return value


## check for valid host (IP or hostname)
def hostCheck(host):
    allowed = set(string.ascii_lowercase + string.digits + ".")
    if set(host) <= allowed:
        pass
    else:
        raise ValueError

## check for valid IP
def ipCheck(host):
    try:
        ip = ipaddress.ip_address(host)
    except:
        raise ValueError


## output handling
def flashOutput(cmdDict, cmd):
    if cmdDict == None:
        flash("Host not available.")

    # output available (therefore no error) --> success/ cmd output
    elif cmdDict[cmd][0]:
        flash(cmdDict[cmd][0])

    # error available --> flash error
    elif cmdDict[cmd][1]:
        flash(cmdDict[cmd][1])

    # no output at all available (poweroff, reboot etc.)
    else:
        flash("No output received from host.")


## Routes

# show control panel and send commands by pushing button
@app.route("/", methods=["GET", "POST"])
def home(name=None):
    if request.method == "GET":
        date = datetime.date.today()

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH(
            [
                "kernel",
                "uptime",
                "ipv4",
                "mac",
                "hostname",
                "check-mariadb",
                "check-mosquitto",
                "check-snmpd",
                "check-ssh",
            ]
        )

        # also output is currently reversed
        # if we later can sort for date by using grok patterns
        # we should easily be able to sort "reversed"
        # to get the newest entry to the top
        try:
            sudoElastic = fetchElastic("search", "file*", "sudo")["hits"]["hits"]
        except:
            sudoElastic = None

        # app.logger.info(statVars['check-mariadb'][0])

        # if host is down then statVars == None
        # no vars -> not alive
        if statVars != None:
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "home.html",
            name=name,
            buttonAlive=buttonAlive,
            date=date,
            interface=interface,
            statVars=statVars,
            sudoElastic=sudoElastic,
        )

    if request.method == "POST":
        outputExecute = doSSH([request.form["cmd"]])
        flashOutput(outputExecute, request.form["cmd"])
        return redirect(url_for("home"))


@app.route("/mysql", methods=["GET", "POST"])
def mysql(name=None):
    if request.method == "GET":

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH(["check-mariadb"])

        try:
            outputElastic = fetchElastic("mysql", "file*")["hits"]["hits"]
        except:
            outputElastic = None

        # if host is down then statVars == None
        if (statVars != None) and (statVars["check-mariadb"][0]):
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "mysql.html",
            name=name,
            buttonAlive=buttonAlive,
            outputElastic=outputElastic,
        )

    if request.method == "POST":
        outputExecute = doSSH([request.form["cmd"]])
        flashOutput(outputExecute, request.form["cmd"])
        return redirect(url_for("mysql"))



@app.route("/snmp", methods=["GET", "POST"])
def snmp(name=None):
    if request.method == "GET":

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH(["check-snmpd"])

        try:
            outputElastic = fetchElastic("mysql", "file*")["hits"]["hits"]
        except:
            outputElastic = None

        # if host is down then statVars == None
        if (statVars != None) and (statVars["check-snmpd"][0]):
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "snmp.html",
            name=name,
            buttonAlive=buttonAlive,
            outputElastic=outputElastic,
        )

    if request.method == "POST":
        outputExecute = doSSH([request.form["cmd"]])
        flashOutput(outputExecute, request.form["cmd"])
        return redirect(url_for("snmp"))


@app.route("/mqtt", methods=["GET", "POST"])
def mqtt(name=None):
    if request.method == "GET":

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH(["check-mosquitto"])

        try:
            outputElastic = fetchElastic("mqtt", "file*")["hits"]["hits"]
        except:
            outputElastic = None

        # if host is down then statVars == None
        if (statVars != None) and (statVars["check-mosquitto"][0]):
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "mqtt.html",
            name=name,
            buttonAlive=buttonAlive,
            outputElastic=outputElastic,
        )

    if request.method == "POST":
        outputExecute = doSSH([request.form["cmd"]])
        flashOutput(outputExecute, request.form["cmd"])
        return redirect(url_for("mqtt"))


@app.route("/ssh", methods=["GET", "POST"])
def sshSite(name=None):
    if request.method == "GET":

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH(["check-ssh"])

        try:
            outputElastic = fetchElastic("ssh", "file*")["hits"]["hits"]
        except:
            outputElastic = None

        # if host is down then statVars == None
        if (statVars != None) and (statVars["check-ssh"][0]):
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "ssh.html",
            name=name,
            buttonAlive=buttonAlive,
            outputElastic=outputElastic,
        )

    if request.method == "POST":
        outputExecute = doSSH([request.form["cmd"]])
        flashOutput(outputExecute, request.form["cmd"])
        return redirect(url_for("sshSite"))


@app.route("/config", methods=["GET", "POST"])
def configure(name=None):
    if request.method == "GET":

        # no error/ output handling needed
        # if we do not get any values just show "n/a" (see info.html)
        statVars = doSSH()

        # read IP of Honeybox from config file
        hostname = readConfigIni("SSH", "hostname")

        # read ports from config ini
        elasticPort = readConfigIni("BEATS", "elasticport")
        kibanaPort = readConfigIni("BEATS", "kibanaport")
        logstashPort = readConfigIni("BEATS", "logstashport")

        # read telegram settings from config ini
        botToken = readConfigIni("TELEGRAM", "bottoken")
        roomID = readConfigIni("TELEGRAM", "roomid")

        # if Auth is set -> pw is configured -> show pw
        useAuth = readConfigIni("BEATS", "useauth")
        if useAuth == "true":
            elasticpw = readConfigIni("BEATS", "elasticpw")
        else:
            elasticpw = None

        honeyboxKeys = []

        # get public and private key
        try:
            pPub = open("honeyboxKey.pub", "r")
            pPriv = open("honeyboxKey", "r")
            honeyboxKeys.append(pPub.read())
            honeyboxKeys.append(pPriv.read())
        except:
            honeyboxKeys.append(None)
            honeyboxKeys.append(None)

        # if host is down then statVars == False (because no parameter)
        if statVars != False:
            buttonAlive = True
        else:
            buttonAlive = False
        return render_template(
            "config.html",
            name=name,
            buttonAlive=buttonAlive,
            honeyboxKeys=honeyboxKeys,
            hostname=hostname,
            elasticpw=elasticpw,
            elasticPort=elasticPort,
            kibanaPort=kibanaPort,
            logstashPort=logstashPort,
            botToken=botToken,
            roomID=roomID,
        )

    if request.method == "POST":

        app.logger.info("%s", request.form)

        # change IP of Pi inside config file
        if request.form["action"] == "set-pi-ip":

            ipPI = str(request.form["ip-pi"])

            try:
                ipCheck(ipPI)
                writeConfigIni("SSH", "hostname", ipPI)
            except:
                flash("Fatal error - bad input value!")

            return redirect(url_for("configure"))


        # set public key
        elif request.form["action"] == "set-pk":

            # check if key is already created
            if not os.path.exists("honeyboxKey"):

                # copy public key into authorized_keys on Honeybox
                try:
                    # create new key pair
                    subprocess.run(
                        [
                            "ssh-keygen",
                            "-t",
                            "ed25519",
                            "-f",
                            "honeyboxKey",
                            "-q",
                            "-N",
                            "",
                        ]
                    )

                    # reread hostname -> we need this for the next cmd
                    hostname = readConfigIni("SSH", "hostname")

                    # ssh-copy-id pub key
                    # subprocess.run(["echo honeybox | sshpass ssh-copy-id -f -i honeyboxKey.pub ubuntu@192.168.0.129 -p 2122 -o StrictHostKeyChecking=no"], shell=True, check=True)
                    subprocess.run(
                        [
                            "echo "
                            + str(password)
                            + " | sshpass ssh-copy-id -f -i honeyboxKey.pub "
                            + str(username)
                            + "@"
                            + str(hostname)
                            + " -p "
                            + str(port)
                            + " -o StrictHostKeyChecking=no"
                        ],
                        shell=True,
                        check=True,
                    )

                    # disable password login (client)
                    writeConfigIni("SSH", "usePassword", "false")

                    # change default password (generate new),
                    # disable pw auth (sshd, server) and clear history
                    doSSH(["change-pw", "disable-pw-auth", "clear-history"])

                    flash("Key successfully created and copied.")

                except:
                    flash("Fatal error - could not create and copy public key!")

            else:
                flash("Fatal error - keys already exist!")

            return redirect(url_for("configure"))


        elif request.form["action"] == "set-ports":

            # read the ports from the form input
            elasticPort = str(request.form["port-elasticsearch"])
            kibanaPort = str(request.form["port-kibana"])
            logstashPort = str(request.form["port-logstash"])

            try:
                if (
                    elasticPort.isdigit()
                    and kibanaPort.isdigit()
                    and logstashPort.isdigit()
                ):

                    # write them to the configIni
                    writeConfigIni("BEATS", "elasticport", elasticPort)
                    writeConfigIni("BEATS", "kibanaport", kibanaPort)
                    writeConfigIni("BEATS", "logstashport", logstashPort)

                    # config the ports of the services (server)
                    # TODO: TEST!
                    subprocess.run(
                        [
                            "sudo",
                            "/home/ubuntu/honeybox/InstallELK.sh",
                            "headless",
                            "config",
                            "ports",
                            str(elasticPort),
                            str(kibanaPort),
                            str(logstashPort)
                        ]
                    )

                    flash("Ports successfully configured.")

                else:
                    raise ValueError

            except ValueError:
                flash("Fatal error - bad input values!")

            except:
                flash("Fatal error - could not set ports!")

            return redirect(url_for("configure"))


        elif request.form["action"] == "set-host":

            server = str(request.form["ip-server"])
            hostType = str(request.form["elk-server"])
            elkHTTPS = str(request.form["elk-https"])

            try:
                # should we check if every parameter is set --> check later with burp
                if server and hostType and elkHTTPS:

                    if hostType == "ipv4":
                        ipCheck(server)
                        writeConfigIni("BEATS", "usehttps", "false")

                    # hostType == 'hostname'
                    else:
                        hostCheck(server)

                        if elkHTTPS == "on":
                            writeConfigIni("BEATS", "usehttps", "true")
                            subprocess.run(
                                [
                                    "sudo",
                                    "/home/ubuntu/honeybox/InstallELK.sh",
                                    "headless",
                                    "config",
                                    "tls",
                                    str(server)
                                ]
                            )
                        else:
                            writeConfigIni("BEATS", "usehttps", "false")

                    writeConfigIni("BEATS", "server", server)
                    flash("Host successfully configured.")

                else:
                    raise ValueError

            except ValueError:
                flash("Fatal error - bad input values!")

            except:
                flash("Fatal error - could not set host!")

            return redirect(url_for("configure"))


        elif request.form["action"] == "set-auth":
            try:
                writeConfigIni("BEATS", "useauth", "true")
                subprocess.run(
                    ["sudo", "/home/ubuntu/honeybox/InstallELK.sh", "headless", "config", "auth"]
                )
                # felix's cmd does already write into the config.ini
                flash("Auth successfully configured.")

            except:
                flash("Fatal error - could not set auth!")

            return redirect(url_for("configure"))

        
        elif request.form["action"] == "set-telegram":

            botToken = str(request.form["bot-token"])
            roomID = str(request.form["room-id"])

            try:
                if not re.match("^[0-9]{9}:[a-zA-Z0-9\-\_]{35}$", botToken):
                    raise ValueError
                
                if not re.match("^-[0-9]{9}$", roomID):
                    raise ValueError

                # sudo ./InstallLELK.sh headless config telegram <bottoken> <roomid>
                subprocess.run(
                    ["sudo", "/home/ubuntu/honeybox/InstallELK.sh", "headless", "config", "telegram", str(botToken), str(roomID)]
                )

                writeConfigIni("TELEGRAM", "bottoken", botToken)
                writeConfigIni("TELEGRAM", "roomid", roomID)

                flash("Telegram alert successfully configured.")

            except ValueError:
                flash("Fatal error - bad input values!")

            except:
                flash("Fatal error - could not set Telegram alert!")

            return redirect(url_for("configure"))


        elif request.form["action"] == "config-pi":
            try:
                # push certs if set (https on)
                useHTTPS = readConfigIni("BEATS", "usehttps")
                if useHTTPS == "true":
                    copyKeys = {
                        "ca.crt" : ["/home/ubuntu/honeybox/ca/ca.crt", "/home/ubuntu/ca/ca.crt"],
                        "filebeat.crt" : ["/home/ubuntu/honeybox/ca/filebeat.crt", "/home/ubuntu/ca/filebeat.crt"],
                        "filebeat.p8"  : ["/home/ubuntu/honeybox/ca/filebeat.p8", "/home/ubuntu/ca/filebeat.p8"],
                    }
                    doSSH(sftp="true", pushFiles=copyKeys)

                # config all (set ports, hostname, toggle auth and https, set pw)    
                outputBeatsConfig = doSSH(["set-beats"])

                flash("Pi successfully configured.")

            except:
                flash("Fatal error - could not config Pi!")

            return redirect(url_for("configure"))
