import paramiko

hostname = "192.168.0.45"
username = "root"
password = "XXX"


ssh = paramiko.SSHClient()


def saveExecution(cmd):
    """Returns SSH output (3-tuple with stdin, 
    stdout and stderr) of the executed command over SSH"""
    return ssh.exec_command(cmd)


def executeCmd(cmd):
    """Check for a know command and execute it
    by calling saveExecution.
    Returns SSH output as 3-tuple."""
    if cmd == "update":
        saveExecution("apt update")
        return saveExecution("apt upgrade -y")

    elif cmd == "off":
        return saveExecution("poweroff")

    elif cmd == "reboot":
        return saveExecution("reboot")

    elif cmd == "system":
        return saveExecution("uname -r")


def doSSH(cmd="no"):
    """Login on remote host with SSH. If no parameter is provided
    then only do an alive-check. Otherwise check if the 
    command is known and remap it using executeCmd.
    Returns SSH output as 3-tuple or alive-value.
    """
    outputSSH = None
    alive = False
    try:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # guess we need root login for low-level functions like poweroff
        # best way would be to at least use identify files (priv keys)
        ssh.connect(hostname=hostname, username=username, password=password, timeout=2)
        if cmd != "no":
            stdin, stdout, stderr = executeCmd(cmd)
            sshOutput = stdout.read().decode("utf-8")
            sshError = stderr.read().decode("utf-8")
            outputSSH = [sshOutput, sshError]

        # no cmd, just check for connection/ if host is alive
        else:
            if ssh.get_transport().is_active():
                alive = True
            else:
                alive = False

    except Exception as e:
        print(e)

    finally:
        # we can not get the output if the session is closed :/
        # TODO: fix
        ssh.close()
        if cmd != "no":
            return outputSSH
        else:
            return alive


system = doSSH("system")[0]

