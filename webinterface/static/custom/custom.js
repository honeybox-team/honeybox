// disabled buttons if one button is pressed and show spinner animation
function spinUpdate(clickedID) {

    // fix race condition? (button disabled before submit)
    $(document).ready(function () {

        var addSpan = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';
        var newText = $('#' + clickedID).text() + ' ';

        // show spinner
        $('#' + clickedID).text(newText)
        $('#' + clickedID).append(addSpan);

        // disable all cmd-buttons (you know, DAU...)
        $(":button[name='cmd']").not(".status-check").prop('disabled', true);
        // $('#' + clickedID).prop('disabled', false);
        // $(":button[name='cmd']").disabled = true;

    });
}


// document is ready/ loaded
$(document).ready(function() {

    // trigger toast message
    var messages = "{{ get_flashed_messages() }}";

    if (typeof messages != 'undefined' && messages != '[]') {
        $('.toast').toast('show');
    };

});